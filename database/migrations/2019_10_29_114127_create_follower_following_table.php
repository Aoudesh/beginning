<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowerFollowingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     DB::statement('SET FOREIGN_KEY_CHECKS=0;');
       Schema::create('follower_following', function (Blueprint $table) {
        $table->integer('follower_id')->unsigned();
        $table->integer('following_id')->unsigned();
        $table->primary(array('follower_id', 'following_id'));

        $table->foreign('follower_id')->references('id')->on('users')
            ->onDelete('restrict')
            ->onUpdate('restrict');

        $table->foreign('following_id')->references('id')->on('users')
            ->onDelete('restrict')
            ->onUpdate('restrict');
    });
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('follower_following');
    }
}
