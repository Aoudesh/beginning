<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnauthenticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('unauthentics', function (Blueprint $table) {
            $table->bigIncrements('unauthenticid');
            $table->string('about')->unique('about');
            $table->string('file_path')->nullable();
            $table->string('extension')->nullable();
            $table->string('type')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('files_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('files_id')->references('id')->on('files')
               ->onDelete('restrict')
               ->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')
              ->onDelete('restrict')
              ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unauthentics');
    }
}
