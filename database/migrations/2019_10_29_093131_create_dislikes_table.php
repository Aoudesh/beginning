<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDislikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('dislikes', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->integer('files_id')->unsigned();
            $table->timestamps();

            $table->foreign('files_id')->references('id')->on('files')
               ->onDelete('restrict')
               ->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')
              ->onDelete('restrict')
              ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dislikes');
    }
}
