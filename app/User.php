<?php

namespace App;
use App\Files;
use App\Comment;
use App\Authenticaton;
use App\Unauthentic;
use App\Like;
use App\Dislike;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements MustVerifyEmail
{

    use Notifiable, HasApiTokens;
     
      use SoftDeletes;
      
     const VERIFIED_USER = '1';
     const UNVERIFIED_USER = '0';

     const AUTHENTIC_USER  = '1';
     const UNAUTHENTIC_USER = '0';

     const ADMIN_USER = 'true';
     const REGULAR_USER = 'false';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'uname', 'email', 'password', 'profile_pic', 'verified', 'authentic', 'admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setNameAttribute($name){
        $this->attributes['name'] = ucfirst($name);
    }

    public function setEmailAttribute($email){
        $this->attributes['email'] = strtolower($email);
    }
     public function setuNameAttribute($uname){
        $this->attributes['uname'] = ucfirst($uname);
    }

  
     
     public function isVerified()
     {
        return $this->verified == User::VERIFIED_USER;
     }

     public function isAuthentic()
     {
        return $this->authentic == User::AUTHENTIC_USER;
     }

     public function isAdmin()
     {
        return $this->admin == User::ADMIN_USER;
     }

     // public function sendpasswordResetNotification($token){
     //    $this->notify(new PasswordResetNotification($token));
     // }

public function followers()
{
    return $this->belongsToMany('App\User', 'follower_following', 'following_id', 'follower_id')
        ->select('id', 'uname', 'name','profile_pic','verified','authentic','admin');
}


public function following()
{
    return $this->belongsToMany('App\User', 'follower_following', 'follower_id', 'following_id')
        ->select('id', 'uname', 'name','profile_pic','verified','authentic','admin');
}

public function files(){
   return $this->hasMany('App\Files','user_id','id');
 }

 public function comment(){
   return $this->hasManyThrough(Comment::class);
 }

  public function authenticaton(){
   return $this->hasManyThrough(Authenticaton::class);
 }
   public function unauthentic(){
   return $this->hasManyThrough(Unauthentic::class);
 }
    public function likes(){
   return $this->hasManyThrough(Like::class);
 }
    public function dislikes(){
   return $this->hasManyThrough(Dislike::class);
}
}
