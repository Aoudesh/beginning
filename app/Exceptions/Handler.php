<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Traits\ApiResponser;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Database\QueryException;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class Handler extends ExceptionHandler
{
        use ApiResponser; 

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();
        return $this->errorResponse($errors,422);
    }
    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception,$request);
        }
        if($exception instanceof ModelNotFoundException){
            $modelname = strtlower(class_basename($exception->getModel()));
            return $this->errorResponse("Does not exist any {$modelname} with the Specified Identifier ",404);
        }
              if ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request,$exception);
        }
          if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse('The Specified Method for Loop is Invalid', 404);
        }
                //   if ($exception instanceof FatalThrowableError) {
        //     return $this->errorResponse('The Specified Method for the request is Invalid', 404);
        // }
         if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse('The Specified URL Cannot Be Found', 404);
        }
             // if ($exception instanceof QueryException){
       //  $errorCode = $exception->errorInfo[1];
       //  if (errorCode == 1451) {
       //      return $this->errorResponse('Canoot remove this resource permenentatly. It is related with any other resource ', 409);
       //  }
       // } 

       if (config('app.debug')){
        return parent::render($request, $exception);
       }

       return $this->errorResponse('unexpected Exception. Try Later', 500); 
       
        return parent::render($request, $exception);
    }
     

    protected function unauthenticated($request, AuthenticationException $exception){
        return $this->errorResponse('Please Signin.', 401);
    }

}
