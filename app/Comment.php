<?php

namespace App;
use App\User;
use App\Files;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Comment extends Model
{
		    use SoftDeletes;
    protected $primaryKey = 'commentid';

  public function setBodyAttribute($body){
        $this->attributes['body'] = ucfirst($body);
    }
       public function user()
    {
        return $this->belongsTo(User::class);
    }

       public function files()
    {
        return $this->belongsToMany(Files::class);
    }

    
//     public function followers()
// {
//     return $this->belongsToMany('App\User', 'follower_following', 'following_id', 'follower_id')
//         ->select('id', 'username', 'name','uid');
// }
}
