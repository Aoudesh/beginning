<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Files;

class Unauthentic extends Model
{
    	    use SoftDeletes;
    protected $primaryKey = 'unauthenticid';
    	       protected $fillable = ['about'];


           public function user()
    {
        return $this->belongsTo(User::class);
    }

       public function files()
    {
        return $this->belongsToMany(Files::class);
    }
}