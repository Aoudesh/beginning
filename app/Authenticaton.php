<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Files;

class Authenticaton extends Model
{
    	    use SoftDeletes;
    protected $primaryKey = 'authenticationid';
    	       protected $fillable = ['about'];


           public function user()
    {
        return $this->belongsTo(User::class);
    }

       public function files()
    {
        return $this->belongsToMany(Files::class);
    }
}


