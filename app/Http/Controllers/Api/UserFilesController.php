<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Files;
use App\Comment;
use App\Authenticaton;
use App\Unauthentic;
use App\Like;
use App\Dislike;


class UserFilesController extends Controller
{
      public function index($id){
      $users = User::findOrFail($id);
      $files =$users->files;
          return response()->json(['data' => $files], 200,[],JSON_NUMERIC_CHECK);
       
        }
//         user
// comment
// authenticaton
// unauthentic
// like
// dislike
      public function fileswithuser(){
       
       $files = Files::withCount('comment')
                ->withCount('authenticaton')
                ->withCount('unauthentic')
                ->withCount('like')
                ->withCount('dislike')
                ->with('user')
                ->orderBy('created_at','DESC')
                ->paginate(10);

       return response()->json(['data' => $files], 200,[],JSON_NUMERIC_CHECK);
      }


      public function authenticatonwithuser(){
       $authen = Authenticaton::with('user')
                ->orderBy('created_at','DESC')
                ->paginate(10);
       return response()->json(['data' => $authen], 200,[],JSON_NUMERIC_CHECK);

      }
      
       public function unauthenticatonwithuser(){
       $unauthen = Unauthentic::with('user')
                ->orderBy('created_at','DESC')
                ->paginate(10);
       return response()->json(['data' => $unauthen], 200,[],JSON_NUMERIC_CHECK);

      }
        public function commentwithuser(){
       $comment = Comment::with('user')
                ->orderBy('created_at','DESC')
                  ->paginate(10);
       return response()->json(['data' => $comment], 200,[],JSON_NUMERIC_CHECK);

      }
        public function likewithuser(){
       $like = Like::with('user')
                ->orderBy('created_at','DESC')
                  ->paginate(10);
       return response()->json(['data' => $like], 200,[],JSON_NUMERIC_CHECK);

      }
        public function dislikewithuser(){
       $like = Dislike::with('user')
                ->orderBy('created_at','DESC')
                 ->paginate(10);
       return response()->json(['data' => $like], 200,[],JSON_NUMERIC_CHECK);

      }

}
