<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Files;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class CommentController extends Controller
{
   
  public function comment(Request $request , $id ){
    
    $this->validate($request,[
        'body'    =>'required|max:255'
    ]);

    $files = Files::findorFail($id);

    $body = $request->body;
      $comment = new Comment();
      $comment->files_id = $files->id;
      $comment->body = $body;
      $comment ->user_id = Auth::user()->id;

      $comment ->save();
      return response()->json(['data' => $comment], 200,[],JSON_NUMERIC_CHECK);

  }

  public function update (Request $request, $id, $commentid){
     $this->validate($request,[
        'body'    =>'required|max:255'
    ]);
          $comment = Comment::find($commentid);
 if ( Auth::user()->id == $comment->user_id){  
     $files = Files::findorFail($id);

      $body = $request->body;
     // $comment->files_id = $files->id;
      $comment->body = $body;
      //$comment ->user_id = Auth::user()->id;

      $comment ->save();
      return response()->json(['data' => $comment], 200,[],JSON_NUMERIC_CHECK);
    }else{
              return response()->json(['Error'=>'Forbidden'],403); 

    }
  }

  public function delete($commentid){
     $comment = Comment::find($commentid);
     if (Auth::user()->id == $comment->user_id){  
      if (is_null($comment)) {
          return response()->json(['Error'=>'comment not found']); 
     }
     $comment ->delete();
     return response()->json(['sucess'=>'sucessfully Deleted']);
    }else{
                      return response()->json(['Error'=>'Access Forbidden'],403); 

    }
  }
     public function index($id){
     $files = Files::findorFail($id);
     $comment = $files->comment;
     return response()->json(['data' => $comment], 200,[],JSON_NUMERIC_CHECK);
       
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Authenticaton  $authenticaton
     * @return \Illuminate\Http\Response
     */
    public function show($commentid)
    {
     $comment = Comment::find($commentid);
      if (is_null($commentid)) {
         return response()->json(['Error'=>'no Record']); 
      }else{
     return response()->json(['data' => $comment], 200,[],JSON_NUMERIC_CHECK);
      }
    }



}
