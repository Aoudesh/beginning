<?php

namespace App\Http\Controllers\Api;

use App\Like;
use App\Files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;



class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
                  $files = Files::find($id);
          $like = $files->like;
          return response()->json(['data' => $like], 200,[],JSON_NUMERIC_CHECK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {

    $files = Files::find($id);
      $like = new Like();
      $like->files_id = $files->id;
      $like ->user_id = Auth::user()->id;

      $like ->save();
      return response()->json(['data' => $like], 200,[],JSON_NUMERIC_CHECK);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $like = Like::find($id);
      if (is_null($like)) {
         return response()->json(['Error'=>'no Record']); 
      }else{
     return response()->json(['data' => $like], 200,[],JSON_NUMERIC_CHECK);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $like = Like::find($id);
    if ( Auth::user()->id == $like->user_id){  
      if (is_null($like)) {
          return response()->json(['Error'=>'no Record']); 
      }
      $like ->delete();
      return response()->json(['sucess'=>'sucessfully Deleted']);
     }else{
                return response()->json(['Error'=>'Access Forbidden'],403); 
     }
    }
}
