<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Files;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Http\Controllers\ApiController;

class FilesController extends ApiController{
public function store(Request $request)
    {
        // validation
        $this->validate($request, [
            'title' => 'required|unique:files,title|min:15|max:40',
            'description' => 'nullable|min:25|max:50',
            'file' => 'nullable|file|mimes:jpeg,jpg,png,gif,mp4,mpeg|max:40240'
        ]); 
          $file = new Files;
         // code for upload 'file'
           $title = $request->title;
           $description = $request->description;

          if($request->hasFile('file')){
            $uniqueid=uniqid();
            $original_name=$request->file('file')->getClientOriginalName();
            $type=$request->file('file')->getClientMimeType();
            $fileextension=$request->file('file')->getClientOriginalExtension();
            $name=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
            $imagepath=url('/storage/upload/files/'.$name);
            $path=$request->file('file')->storeAs('public/upload/files/',$name);  
            $file->file_path = $imagepath;
            $file->extension = $fileextension;
            $file->type = $type;
           }

        // $file = new Files;
         $file->title = $title;
         $file->description = $description;
         $file->user_id = Auth::user()->id;
         $file->save();
         // return response()->json(['data' => $file], 200,[],JSON_NUMERIC_CHECK);
         return $this->showOne($file);

        // dd($request->all());
 }
 public function index(){
  $files = Files::all();
  //return response()->json(['data'=> $files],200,[],JSON_NUMERIC_CHECK);
  return $this->showAll($files); 
 }
 public function show($id){
  $files = Files::find($id);
  // return response()->json(['data'=> $files],200,[],JSON_NUMERIC_CHECK);
  return $this->showOne($files);
 }
 public function delete($id){
  $files = Files::find($id);
  if (is_null($files)) {
       return response()->json(['Error'=>'Post not found']); 
  }
  $files->delete();
  return response()->json(['sucess'=>'sucessfully Deleted']);
 }
 public function update(Request $request, $id){
        // validation
        $this->validate($request, [
            'title' => 'required|unique:files,title|min:15|max:40',
            'description' => 'nullable|min:25|max:50',
            'file' => 'nullable|file|mimes:jpeg,jpg,png,gif,mp4,mpeg|max:40240'
        ]); 
           $file =  Files::find($id);
           $title = $request->title;
           $description = $request->description;
         // code for upload 'file'

          if($request->hasFile('file')){
            $uniqueid=uniqid();
            $original_name=$request->file('file')->getClientOriginalName();
            $type=$request->file('file')->getClientMimeType();
            $fileextension=$request->file('file')->getClientOriginalExtension();
            $name=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
            $imagepath=url('/storage/upload/files/'.$name);
            $path=$request->file('file')->storeAs('public/upload/files/',$name);  
            $file->file_path = $imagepath;
           }
    
         $file->title = $title;
         $file->description = $description;
         $file->user_id = Auth::user()->id;
         $file->save();
         return response()->json(['data' => $file], 200,[],JSON_NUMERIC_CHECK);
 }
}