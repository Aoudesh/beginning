<?php

namespace App\Http\Controllers\Api;

use App\Authenticaton;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Files;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;



class AuthenticatonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
     $files = Files::findorFail($id);
     $authen = $files->authenticaton;
     return response()->json(['data' => $authen], 200,[],JSON_NUMERIC_CHECK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request , $id )
    {
    $this->validate($request, [
            'about' => 'required|unique:authenticatons,about|min:15|max:40',
            'file' => 'nullable|file|mimes:jpeg,jpg,png,gif,mp4,mpeg|max:5500'
        ]); 
    if ( Auth::user()->isAuthentic() || Auth::user()->isVerified() ) {
         $files = Files::findorFail($id);
         $authen = new Authenticaton();

          $about = $request->about;
          if($request->hasFile('file')){
            $uniqueid=uniqid();
            $original_name=$request->file('file')->getClientOriginalName();
            $type=$request->file('file')->getClientMimeType();
            $fileextension=$request->file('file')->getClientOriginalExtension();
            $name=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
            $path=$request->file('file')->storeAs('public/upload/authen/',$name);  
            $imagepath=url('/storage/upload/authen/'.$name);
            $authen->file_path = $imagepath;
            $authen->extension = $fileextension;
            $authen->type = $type;
           }    
      $authen ->about = $about;
      $authen->files_id = $files->id;
      $authen ->user_id = Auth::user()->id;

      $authen ->save();
      return response()->json(['data' => $authen], 200,[],JSON_NUMERIC_CHECK);
    }else{
                  return response()->json(['Error'=>'You are not Authorized Person to Judge Please verify Yourself']); 

    }
  }

 
    /**
     * Display the specified resource.
     *
     * @param  \App\Authenticaton  $authenticaton
     * @return \Illuminate\Http\Response
     */
    public function show($authenticatonid)
    {
     $authen = Authenticaton::find($authenticatonid);
      if (is_null($authen)) {
         return response()->json(['Error'=>'no Record']); 
      }else{
     return response()->json(['data' => $authen], 200,[],JSON_NUMERIC_CHECK);
      }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authenticaton  $authenticaton
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $authenticatonid)
    {
        $this->validate($request, [
            'about' => 'required|unique:authenticatons,about|min:15|max:40',
            'file' => 'nullable|file|mimes:jpeg,jpg,png,gif,mp4,mpeg|max:5500'
        ]); 
        $authen = Authenticaton::find($authenticatonid);
        if ( Auth::user()->id == $authen->user_id){  
         if ( Auth::user()->isAuthentic() || Auth::user()->isVerified() ) {
         $files = Files::findorFail($id);
          $about = $request->about;
          if($request->hasFile('file')){
            $uniqueid=uniqid();
            $original_name=$request->file('file')->getClientOriginalName();
            $type=$request->file('file')->getClientMimeType();
            $fileextension=$request->file('file')->getClientOriginalExtension();
            $name=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
            $path=$request->file('file')->storeAs('public/upload/authen/',$name);  
            $imagepath=url('/storage/upload/authen/'.$name);
            $authen->file_path = $imagepath;
            $authen->extension = $fileextension;
            $authen->type = $type;
           }    
      $authen ->about = $about;
      //$authen->files_id = $files->id;
     //$authen ->user_id = Auth::user()->id;
      $authen ->save();
      return response()->json(['data' => $authen], 200,[],JSON_NUMERIC_CHECK);
    }else{
                  return response()->json(['Error'=>'You are not Authorized Person to Judge Please verify Yourself']); 
     }

        }else{
        return response()->json(['Error'=>'Access Forbidden'],403); 
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authenticaton  $authenticaton
     * @return \Illuminate\Http\Response
     */
    public function destroy($authenticatonid)
    {
             $authen = Authenticaton::find($authenticatonid);
    if ( Auth::user()->id == $authen->user_id){  
      if (is_null($authen)) {
          return response()->json(['Error'=>'no Record']); 
      }
      $authen ->delete();
      return response()->json(['sucess'=>'sucessfully Deleted']);
     }else{
                return response()->json(['Error'=>'Access Forbidden'],403); 
     }
    }
}
