<?php

namespace App\Http\Controllers\Api;

use App\Dislike;
use App\Files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class DislikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index($id)
    {
          $files = Files::find($id);
          $dislike = $files->dislike;
          return response()->json(['data' => $dislike], 200,[],JSON_NUMERIC_CHECK);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {

    $files = Files::find($id);
      $dislike = new Dislike();
      $dislike->files_id = $files->id;
      $dislike ->user_id = Auth::user()->id;

      $dislike ->save();
      return response()->json(['data' => $dislike], 200,[],JSON_NUMERIC_CHECK);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dislike  $dislike
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dislike = Dislike::find($id);
      if (is_null($dislike)) {
         return response()->json(['Error'=>'no Record']); 
      }else{
     return response()->json(['data' => $dislike], 200,[],JSON_NUMERIC_CHECK);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dislike  $dislike
     * @return \Illuminate\Http\Response
     */
       public function destroy($id)
    {
    $dislike = Dislike::find($id);
    if ( Auth::user()->id == $dislike->user_id){  
      if (is_null($dislike)) {
          return response()->json(['Error'=>'no Record']); 
      }
      $dislike ->delete();
      return response()->json(['sucess'=>'sucessfully Deleted']);
     }else{
       return response()->json(['Error'=>'Access Forbidden'],403); 
     }
    }
}
