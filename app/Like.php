<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Files;

class Like extends Model
{ 
  
           public function user()
    {
        return $this->belongsTo(User::class);
    }

       public function files()
    {
        return $this->belongsToMany(Files::class);
    }
}
