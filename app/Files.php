<?php

namespace App;
use App\User;
use App\Comment;
use App\Authenticaton;
use App\Unauthentic;
use App\Like;
use App\Dislike;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;


class Files extends Model
{
	    use SoftDeletes;
	       protected $fillable = ['title'];


	  public function setTitleAttribute($title){
        $this->attributes['title'] = ucfirst($title);
    }
      public function setdescriptionAttribute($description){
        if(is_null($description)){
            $this->attributes['description'] = null;
        }
        else{
        $this->attributes['description'] = ucfirst($description);
    }
 }

   public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function comment(){
    return $this->hasMany(Comment::class);
    }
    
    public function authenticaton(){
    return $this->hasMany(Authenticaton::class);
    }
      public function unauthentic(){
    return $this->hasMany(Unauthentic::class);
    }

          public function like(){
    return $this->hasMany(Like::class);
    }
          public function dislike(){
    return $this->hasMany(Dislike::class);
    }
}
