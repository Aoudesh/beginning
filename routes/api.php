<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register','Api\Auth\RegisterController@register');
Route::post('login','Api\Auth\LoginController@login');
Route::post('refresh','Api\Auth\LoginController@refresh');
//email//
Route::get('/email/resend', 'Api\Auth\VerificationController@resend')->name('verification.resend');
Route::get('/email/verify/{id}/{hash}', 'Api\Auth\VerificationController@verify')->name('verification.verify');
//email//

//passwordreset & forget-password//

Route::post('/password/email', 'Api\Auth\ForgotPasswordController@sendResetLinkEmail');;
//password//

Route::middleware('auth:api')->group(function () {
     
     //email verification check//
    Route::post('email/verify', 'Api\Auth\VerificationController@show')->name('verification.notice');
    //email//


    Route::post('logout', 'Api\Auth\LoginController@logout');
    Route::get('details','Api\Auth\LoginController@details');
    Route::patch('/user/update','Api\Auth\LoginController@update');
    Route::get('/getUser','Api\UserresourceController@getUser');
    Route::resource('users', 'Api\UserresourceController')->only([
    'index', 'show']);
  
   // Route::get('/users/show/{id}','Api\UserresourceController@show');
    Route::get('/getUserall','Api\UserresourceController@getUserall');//ye admin klia


    /// file///
     Route::resource('files','Api\FilesController')->only(['index','show']);
    Route::post('store','Api\FilesController@store');
    Route::get('/user/{id}','Api\UserresourceController@user');
    //Route::get('index','Api\FileController@index');
    //Route::get('/show/{id}','Api\FileController@show');
    Route::delete('/delete/{id}','Api\FilesController@delete');
    Route::patch('/update/{id}','Api\FilesController@update');
    //// file ////

//usersfollow////
    Route::post('users/follow/{id}', 'Api\UserresourceController@action');
    Route::get('users/{id}','Api\UserresourceController@show');
    Route::get('users','Api\UserresourceController@index');
///usersfollow///


      Route::post('users/{id}/adminpanel', 'Api\UserresourceController@adminpanel'); 
      Route::post('users/{id}/partaction','Api\UserresourceController@partaction');
      /// comment //


      Route::post('comment/files/{id}','Api\CommentController@comment');
      Route::patch('/files/{id}/comment/{commentid}','Api\CommentController@update');
      Route::delete('comment/{commentid}','Api\CommentController@delete');
     Route::get('/files/{id}/comment','Api\CommentController@index');
       Route::get('/comment/{commentid}','Api\CommentController@show');

      /// comment ///
      Route::get('/user/{id}/','Api\UserFilesController@index');

      //authentication//
      Route::post('/authen/files/{id}/','Api\AuthenticatonController@create');
      Route::get('/authen/files/{id}','Api\AuthenticatonController@index');
      Route::post('/files/{id}/authen/{authenticatonid}','Api\AuthenticatonController@update');//update for image too 
       Route::delete('/authen/{authenticatonid}','Api\AuthenticatonController@destroy');
       Route::get('/authen/{authenticatonid}','Api\AuthenticatonController@show');

      //authentication//

      //unauthentic//
      Route::post('/unauthentic/{id}/','Api\UnauthenticController@create');
      Route::get('/unauthentic/files/{id}','Api\UnauthenticController@index');
      Route::post('/files/{id}/unauthentic/{unauthenticid}','Api\UnauthenticController@update');//update for image too 
       Route::delete('/unauthentic/{unauthenticid}','Api\UnauthenticController@destroy');
       Route::get('/unauthentic/{unauthenticid}','Api\UnauthenticController@show');

      //unauthentic//
     
     //like//
     Route::post('/like/files/{id}','Api\LikeController@create');
     Route::get('/like/{id}','Api\LikeController@show');
     Route::get('/like/files/{id}','Api\LikeController@index');
     Route::delete('/like/{id}','Api\LikeController@destroy');
    //like//
    
    //Dislike//
     Route::post('/dislike/files/{id}','Api\DislikeController@create');
     Route::get('/dislike/{id}','Api\DislikeController@show');
     Route::get('/dislike/files/{id}','Api\DislikeController@index');
     Route::delete('/dislike/{id}','Api\DislikeController@destroy');
    //Dislike//

    
    //files with user and count//
     Route::get('files','Api\UserFilesController@fileswithuser');
     Route::get('authenication','Api\UserFilesController@authenticatonwithuser');
     Route::get('unauthenication','Api\UserFilesController@unauthenticatonwithuser');
     Route::get('comment','Api\UserFilesController@commentwithuser');
     Route::get('like','Api\UserFilesController@likewithuser');
     Route::get('dislike','Api\UserFilesController@dislikewithuser');




});
